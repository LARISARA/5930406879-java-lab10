package leelawattananonkul.arisara.lab10;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import java.awt.geom.Ellipse2D;

public class Ball extends Ellipse2D.Double{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int r;
	
	public Ball(int x, int y, int r) {
		super(x, y, 2*r, 2*r);
		this.r = r;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}
	
}