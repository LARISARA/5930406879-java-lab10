package leelawattananonkul.arisara.lab10;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import java.awt.*;
import javax.swing.*;

public class SimpleGameWindow extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4502101666088333454L;
	
	public static final int WIDTH = 650;
	public static final int HEIGHT = 500;
	
	public SimpleGameWindow(String string) {
		super(string);
	}
	
	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (dim.width - WIDTH)/2;
		int y = (dim.height - HEIGHT)/2;
		setLocation(x,y);
	    setVisible(true);
	    setSize(new Dimension(WIDTH+1,HEIGHT+23));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void createAndShowGUI() {
		SimpleGameWindow window = new SimpleGameWindow("My Simple Game Window");
		window.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}	
	
}