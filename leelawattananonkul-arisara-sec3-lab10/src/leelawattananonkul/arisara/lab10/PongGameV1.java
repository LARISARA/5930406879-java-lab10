package leelawattananonkul.arisara.lab10;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import javax.swing.*;

public class PongGameV1 extends SimpleGameWindow{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PongGameV1(String string) {
		super(string);
	}

	public static void createAndShowGUI() {
		PongGameV1 window = new PongGameV1("CoE Pong Game V1");
		window.addComponents();
		window.setFrameFeatures();
	}
	
	private void addComponents() {
		setContentPane(new PongGamePanel());
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}