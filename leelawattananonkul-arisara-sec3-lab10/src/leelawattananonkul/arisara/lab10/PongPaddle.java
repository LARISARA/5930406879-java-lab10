package leelawattananonkul.arisara.lab10;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import java.awt.geom.Rectangle2D;

public class PongPaddle extends Rectangle2D.Double{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7275823349775680481L;
	
	public static int HEIGHT = 80;
	public static int WIDTH = 15;
	
	public PongPaddle(int x, int y, int w, int h) {
		super(x,y,w,h);
	}

	public int getW() {
		return (int) this.getWidth();
	}


	public int getH() {
		return (int) this.getHeight();
	}

}