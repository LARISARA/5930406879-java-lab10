package leelawattananonkul.arisara.lab10;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.util.*;
import javax.swing.*;

public class MovingPongGamePanelV3 extends JPanel implements Runnable, KeyListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected MovingBall movingBall;
	private Thread running;
	private Random rand;
	private int ballR = 20, axis_x, axis_y;
	protected MovablePongPaddle movableRightPad;
	protected MovablePongPaddle movableLeftPad;
	protected Rectangle2D.Double box;
	protected Integer player1Score;
	protected Integer player2Score;

	public MovingPongGamePanelV3() {
		super();

		addKeyListener(this);
		setFocusable(true);

		setBackground(Color.BLACK);

		movableLeftPad = new MovablePongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH,
				PongPaddle.HEIGHT);
		movableRightPad = new MovablePongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH,
				SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);
		
		resetBall();
	
		movingBall = new MovingBall(SimpleGameWindow.WIDTH/2 - ballR, SimpleGameWindow.HEIGHT/2 - ballR, ballR, axis_x, axis_y);
		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);
		
		player1Score = 0;
		player2Score = 0;

		running = new Thread(this);
		running.start();
	}

	private void resetBall() {
		rand = new Random();
		axis_x = rand.nextInt(7) - 3;
		axis_y = rand.nextInt(7) - 3;
		
		if (axis_x == 0) {
			resetBall();
		}
		if (axis_y == 0) {
			resetBall();
		}
		
		movingBall = new MovingBall(SimpleGameWindow.WIDTH/2 - ballR, SimpleGameWindow.HEIGHT/2 - ballR, ballR, axis_x, axis_y);
		
		
	}

	@Override
	public void run() {
		
		while (true) {
			moveBall();
			
			repaint();
			this.getToolkit().sync(); 
			
			try {
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
	}

	private void moveBall() {
		movingBall.move();
		
		if (movingBall.y > box.getHeight() - 2*ballR || movingBall.y < 0) {
			movingBall.setVelY(movingBall.getVelY() * -1);
		}
		
		if (movingBall.chkMoveX() + 2*ballR > movableRightPad.x &&
				movingBall.chkMoveY() + 2*ballR >= movableRightPad.y &&
				movingBall.chkMoveY() <= movableRightPad.y + movableRightPad.height) {
			movingBall.setVelX(movingBall.getVelX() * -1);
		}
		
		if (movingBall.chkMoveX() - movableLeftPad.width < movableLeftPad.x &&
				movingBall.chkMoveY() + 2*ballR >= movableLeftPad.y &&
				movingBall.chkMoveY() <= movableLeftPad.y + movableLeftPad.height) {
			movingBall.setVelX(movingBall.getVelX() * -1); 
		}
		
		if (movingBall.x > box.getWidth()) { 
			player1Score ++;
			resetBall();
		}
		
		if (movingBall.x < 0 - 2*ballR) {
			player2Score ++;
			resetBall();
		}
	
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		g2.drawLine(movableLeftPad.getW(), 0, movableLeftPad.getW(), SimpleGameWindow.HEIGHT);

		g2.drawLine(SimpleGameWindow.WIDTH - movableRightPad.getW(), 0, SimpleGameWindow.WIDTH - movableRightPad.getW(),
				SimpleGameWindow.HEIGHT);

		g2.setFont(new Font(Font.SERIF, Font.BOLD, 48));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		
		/*
		 * The game will be over when one of the players reach 10 score.
		 * When the game is over, the message shows the winner's name and the ball moved back to the middle of screen
		 */
		
		if (player1Score == 10) {
			resetBall();
			g2.drawString("Player 1 wins.",(SimpleGameWindow.WIDTH * 2 / 7), SimpleGameWindow.HEIGHT / 3);
		}else if(player2Score == 10) {
			resetBall();
			g2.drawString("Player 2 wins.",(SimpleGameWindow.WIDTH * 2 / 7), SimpleGameWindow.HEIGHT / 3);
		}

		g2.fill(movableLeftPad);
		g2.fill(movableRightPad);

		g2.fill(movingBall);

		g2.draw(box);

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int arg0 = e.getKeyCode();
		if (arg0 == KeyEvent.VK_UP) {
			movableRightPad.moveUp();
		} if (arg0 == KeyEvent.VK_DOWN) {
			movableRightPad.moveDown();
		} 
		int arg1 = e.getKeyCode();
		if (arg1 == KeyEvent.VK_W) {
			movableLeftPad.moveUp();
		} if (arg1 == KeyEvent.VK_S) {
			movableLeftPad.moveDown();
		} 
		
		repaint();
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}