package leelawattananonkul.arisara.lab10;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

public class MovingPongGamePanel extends JPanel implements KeyListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected Integer player1Score;
	protected Integer player2Score;
	protected Ball ball;
	private int ballR = 20; // radius of the ball
	protected Rectangle2D.Double box;
	protected MovablePongPaddle movableLeftPad;
	protected MovablePongPaddle movableRightPad;

	public MovingPongGamePanel() {
		super();

		addKeyListener(this);
		setFocusable(true);

		setBackground(Color.BLACK);

		movableLeftPad = new MovablePongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH,
				PongPaddle.HEIGHT);
		movableRightPad = new MovablePongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH,
				SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);

		ball = new Ball(SimpleGameWindow.WIDTH / 2 - ballR, SimpleGameWindow.HEIGHT / 2 - ballR, ballR);

		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);

		player1Score = 0;
		player2Score = 0;

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		g2.drawLine(movableLeftPad.getW(), 0, movableLeftPad.getW(), SimpleGameWindow.HEIGHT);

		g2.drawLine(SimpleGameWindow.WIDTH - movableRightPad.getW(), 0, SimpleGameWindow.WIDTH - movableRightPad.getW(),
				SimpleGameWindow.HEIGHT);

		g2.setFont(new Font(Font.SERIF, Font.BOLD, 48));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);

		g2.fill(movableLeftPad);
		g2.fill(movableRightPad);
		
		g2.fill(ball);

		g2.draw(box);

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int arg0 = e.getKeyCode();
		if (arg0 == KeyEvent.VK_UP) {
			movableRightPad.moveUp();
		} if (arg0 == KeyEvent.VK_DOWN) {
			movableRightPad.moveDown();
		} 
		int arg1 = e.getKeyCode();
		if (arg1 == KeyEvent.VK_W) {
			movableLeftPad.moveUp();
		} if (arg1 == KeyEvent.VK_S) {
			movableLeftPad.moveDown();
		} 
		
		repaint();
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
