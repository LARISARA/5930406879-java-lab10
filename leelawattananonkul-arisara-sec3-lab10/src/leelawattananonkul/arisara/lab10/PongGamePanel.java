package leelawattananonkul.arisara.lab10;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class PongGamePanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected Ball ball;
	private int ballR = 20;
	protected PongPaddle rightPad;
	protected PongPaddle leftPad;
	protected Integer player1Score;
	protected Integer player2Score;
	private int scoreFontSize = 48;
	protected Rectangle2D.Double box;
	
	
	public PongGamePanel() {

		setBackground(Color.BLACK);

		leftPad = new PongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);
		rightPad = new PongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);

		ball = new Ball(SimpleGameWindow.WIDTH / 2 - ballR, SimpleGameWindow.HEIGHT / 2 - ballR, ballR);
		
		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);

		player1Score = 0;
		player2Score = 0;

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		g2.drawLine(leftPad.getW(), 0, leftPad.getW(), SimpleGameWindow.HEIGHT);

		g2.drawLine(SimpleGameWindow.WIDTH - rightPad.getW(), 0, SimpleGameWindow.WIDTH - rightPad.getW(), SimpleGameWindow.HEIGHT);

		g2.setFont(new Font(Font.SERIF, Font.BOLD, scoreFontSize));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT/5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT/5);

		g2.fill(leftPad);
		g2.fill(rightPad);

		g2.fill(ball);

		g2.draw(box);
	}
}