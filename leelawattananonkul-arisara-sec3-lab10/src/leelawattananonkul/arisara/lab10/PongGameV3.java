package leelawattananonkul.arisara.lab10;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import javax.swing.*;

public class PongGameV3 extends SimpleGameWindow{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PongGameV3(String title) {
		super(title);
	}
	
	public static void createAndShowGUI() {
		PongGameV3 window = new PongGameV3("CoE Pong Game V3");
		window.addComponents();
		window.setFrameFeatures();
	}

	private void addComponents() {
		setContentPane(new MovingPongGamePanelV2());
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
